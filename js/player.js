
class Player{
    gameState;
    playerState;
    canvas;
    name;
    character;
    oppoCharacter;
    opponent;
    attacks;
    oppoAttacks;
    baseBackground;
    middleBackground;
    topBackground;
    items;
    life;
    occupationNum;
    occupationState;
    occupationTime;
    
    constructor(name, src0, src1){
        this.baseBackground = new Background("css/images/map1/background1-0.png");
        this.middleBackground = new Background("css/images/map1/background2-1.png");
        this.topBackground = new Background("css/images/map1/background2-2.png");
        this.gameState = true;
        this.playerState = true;
        this.name = name;
        this.canvas = new Canvas(name);
        this.character = new Character(src0, name, "player", this.baseBackground);
        this.oppoCharacter = new Character(src1, this.oppoName(name), "oppo");
        this.isKeyPressed = false;
        this.attacks = [];
        this.oppoAttacks = [];
        this.items = [];
        this.life = 200;
        this.occupationNum = 0;
        this.occupationState = false;
        this.occupationTime = 90;

    }
    
    nameToColor(){
        if(this.name === "A"){
            return "blue";
        }
        if(this.name === "B"){
            return "red";
        }
    }
    occupationEvent(){
        var ox = this.opponent.oppoCharacter.gx - this.opponent.baseBackground.x;
        var oy = this.opponent.oppoCharacter.gy - this.opponent.baseBackground.y;
        this.canvas.context.fillStyle = this.nameToColor(this.name);
        this.canvas.context.fillRect(this.character.dx - 10, this.character.dy - 20, this.occupationNum / 2, 5);
        this.opponent.canvas.context.fillStyle = this.nameToColor(this.name);
        this.opponent.canvas.context.fillRect(ox - 10, oy - 20, this.occupationNum / 2, 5);
        if(this.occupationNum >= 200){
            this.occupationState = true;
            this.opponent.occupationState = false;
            this.occupationTime = 90;
            this.opponent.occupationTime = 90;
            this.baseBackground.changeImageSource("css/images/map1/background1-0-" + this.nameToColor(this.name) + ".png");
            this.opponent.baseBackground.changeImageSource("css/images/map1/background1-0-" + this.nameToColor(this.name) + ".png");
            var rNum = Math.floor(Math.random() * 9 + 4);
            this.newItem(1160, 760, rNum);
            this.opponent.newItem(1160, 760, rNum);

        }
        else{
            this.occupationNum++;
        }
    }

    characterOccupation(){
        if(!this.occupationState){
            if(this.checkOccupation() && !this.opponent.checkOccupation()){
                this.occupationEvent();
            }
            else{
                this.occupationNum = 0;
            }
        }
        else{
            this.showOccupationTime(this.occupationTime);
            this.opponent.showOccupationTime(this.occupationTime);
        }
    }

    showOccupationTime(t){
        this.canvas.context.font = "30px Arial";
        this.canvas.context.textAlign = "center";
        this.canvas.context.fillStyle = "black";
        this.canvas.context.fillText("Time : " + t, 400, 50)

    }

    checkOccupation(){
        var ox = 1080;
        var oy = 680;
        var ow = 160;
        var oh = 160;
        var cx = this.character.gx + 20;
        var cy = this.character.gy + 40;
        var cw = this.character.dw / 2;
        var ch = this.character.dh / 2;;
        if(((ox + ow) >= cx) && (ox <= (cx + cw)) && ((oy + oh) >= cy) && (oy <= (cy + ch))){
            return true;
        }
        return false;
    }

    occupationTimePass(){
        if(this.occupationState){
            this.occupationTime--;
            if(this.occupationTime <= 0){
                alert("player " + this.name + " won.");
                this.gameState = false;
            }
        }
    }

    gPositionToIndex(gx, gy){
        var x = x + 20;
        var y = y + 40;
        var w = this.character.dw / 2;
        var h = this.character.dh / 2;

        var tempXL = Math.floor(x / 40);
        var tempXR = Math.floor((x + w) / 40);
        var tempYU = Math.floor(y / 40);
        var tempYD = Math.floor((y + 38) / 40);

        if(tempYD === 50){
            tempYD--;
        }
    }

    gameEndEvent(){
        alert("player " + this.oppoName(this.name)+ " won");
        this.gameState = false;
    }

    lifeDecrease(){
        this.life--;
        if(this.life < 0){
            this.gameEndEvent();
        }
    }
    oppoName(name){
        if(name === 'A'){
            return 'B';
        }
        else{
            return 'A';
        }
    }

    // item events
    newItem(gx, gy, num){
        this.items.push(new Item(gx - this.baseBackground.x, gy - this.baseBackground.y, gx, gy, num));
        
    }

    moveItems(x, y){
        x = x * this.character.stats.moveSpeed;
        y = y * this.character.stats.moveSpeed;
        for(var i = 0; i < this.items.length; i++){
            this.items[i].move(x, y);
        }
    }

    itemsPositionReset(){
        for(var i = 0; i < this.items.length; i++){
            this.items[i].reposition(this.baseBackground.x, this.baseBackground.y);
        }
    }

    isItemCollied(item){
        var ix = item.x;
        var iy = item.y;
        var iw = item.width;
        var ih = item.height;
        var cx = this.character.dx;
        var cy = this.character.dy;
        var cw = this.character.dw;
        var ch = this.character.dh;;

        if(((ix + iw) >= cx) && (ix <= (cx + cw)) && ((iy + ih) >= cy) && (iy <= (cy + ch)) && this.playerState){
            return true;
        }
        return false;
    }
    
    setOppoCharacter(character){
        this.character = character;
    }

    isValid(cx, cy, bx, by, gx, gy){
        var checkX = false;
        var checkY = false;
        if(this.isCharacterInsideCanvas(cx, this.character.dy) && this.isCollisionNotOccur(gx, this.character.gy)){
            if(this.isBackgroundInsideCanvas(bx, this.baseBackground.y)){ 
                //x direction event
                if(cx > this.character.dx){
                    if(cx >= this.canvas.WIDTH/2 + 50){
                        this.attacksPositionSetting(-1, 0);
                        this.moveItems(-1, 0);
                        this.baseBackground.x = bx;

                    }
                    else{
                        this.character.dx = cx;
                    }
                }
                else if(cx < this.character.dx){
                    if(cx <= this.canvas.WIDTH/2 - 50){
                        this.attacksPositionSetting(1, 0);
                        this.moveItems(1, 0);
                        this.baseBackground.x = bx;
                    }
                    else{
                        this.character.dx = cx;
                    }
                }
            }
            else{
                this.character.dx = cx;
            }
            checkX = true;
            this.character.gx = gx;
        }
        if(this.isCharacterInsideCanvas(this.character.dx, cy) && this.isCollisionNotOccur(this.character.gx, gy)){
            if(this.isBackgroundInsideCanvas(this.baseBackground.x, by)){
                //y direction evnet
                if(cy > this.character.dy){
                    if(cy >= this.canvas.HEIGHT/2 + 50){
                        this.attacksPositionSetting(0,-1);
                        this.moveItems(0, -1);
                        this.baseBackground.y = by;
                    }
                    else{
                        this.character.dy = cy;
                    }
                }
                else if(cy < this.character.dy){
                    if(cy <= this.canvas.HEIGHT/2 - 50){
                        this.attacksPositionSetting(0, 1);
                        this.moveItems(0, 1);
                        this.baseBackground.y = by;
                    }
                    else{
                        this.character.dy = cy;
                    }
                }
            }
            else{
                this.character.dy = cy;
            }
            checkY = true;
            this.character.gy = gy;
        }
        
        if(!checkX && !checkY){
            return false;
        }
        return true;  
    }

    isCharacterInsideCanvas(x, y){
        if(x >= 0 &&  y >= 0 && x < this.canvas.WIDTH - this.character.sw && y < this.canvas.HEIGHT - this.character.sh){
            return true;
        }
        return false;
    }
    isBackgroundInsideCanvas(x, y){
        if(x >= 0 && y >= 0 && x < this.baseBackground.WIDTH - this.canvas.WIDTH && y < this.baseBackground.HEIGHT - this.canvas.HEIGHT){
            return true;
        }
        return false;
    }
    
    isCollisionNotOccur(x, y){
        return this.characterAndWallNotCollied(x, y) && this.characterAndCharacterNotCollied(x, y);
    }
    itemAndWallNotCollied(x, y){
        var w = 40;
        var h = 40;
        var tempXL = Math.floor(x / 40);
        var tempXR = Math.floor((x + w) / 40);
        var tempYU = Math.floor(y / 40);
        var tempYD = Math.floor((y + h) / 40);
        if(tempYD === 50){
            tempYD--;
        }
        if(this.baseBackground.wallMap[tempYU][tempXL] ||
        this.baseBackground.wallMap[tempYU][tempXR] || 
        this.baseBackground.wallMap[tempYD][tempXL] || 
        this.baseBackground.wallMap[tempYD][tempXR]){
            return false;
        }
        return true;
    }
    characterAndWallNotCollied(x, y){
        var x = x + 20;
        var y = y + 40;
        var w = this.character.dw / 2;
        var h = this.character.dh / 2;

        var tempXL = Math.floor(x / 40);
        var tempXR = Math.floor((x + w) / 40);
        var tempYU = Math.floor(y / 40);
        var tempYD = Math.floor((y + 38) / 40);

        if(tempYD === 50){
            tempYD--;
        }

        if(this.baseBackground.wallMap[tempYU][tempXL] ||
        this.baseBackground.wallMap[tempYU][tempXR] || 
        this.baseBackground.wallMap[tempYD][tempXL] || 
        this.baseBackground.wallMap[tempYD][tempXR]){
            return false;
        }
        return true;
    }

    characterAndCharacterNotCollied(x, y){
        var x = x + 20;
        var y = y + 40;
        var w = this.character.dw / 2;
        var h = this.character.dh / 2;
        var ox = this.oppoCharacter.gx + 20;
        var oy = this.oppoCharacter.gy + 40;
        var ow = this.oppoCharacter.dw / 2;
        var oh = this.oppoCharacter.dh / 2;

        if(((x + w) >= ox) && (x <= (ox + ow)) && ((y + h) >= oy) && (y <= (oy + oh))){
            this.attackHitEventMovedBack(this.character.view)
            return false;
        }
        
        return true;
    }


    isAttackNotCollied(x, y, w, h){
        var tempXL = Math.floor(x / 40);
        var tempXR = Math.floor((x + w) / 40);
        var tempYU = Math.floor(y / 40);
        var tempYD = Math.floor((y + h) / 40);
        if(!this.baseBackground.wallMap[tempYU][tempXL] &&
        !this.baseBackground.wallMap[tempYU][tempXR] && 
        !this.baseBackground.wallMap[tempYD][tempXL] && 
        !this.baseBackground.wallMap[tempYD][tempXR]){
            return true;
        }
        return false;
    }

    isAttackHit(attack, dir){
        var AD = attack.attackDamage;
        var agx = attack.gx;
        var agy = attack.gy;
        var aw = attack.aw;
        var ah = attack.ah;

        var cgx = this.character.gx + 20;
        var cgy = this.character.gy + 40;
        var cw = this.character.dw - 40;
        var ch = this.character.dh - 40;

        var ocgx = this.opponent.character.gx + 20;
        var ocgy = this.opponent.character.gy + 40;
        var ocw = this.opponent.character.dw - 40;
        var och = this.opponent.character.dh - 40;

        var isAttackHitOpponent = false;
        var isAttackHitMyself = false;
        if(((agx + aw) >= ocgx) && (agx <= (ocgx + ocw)) && ((agy + ah) >= ocgy) && (agy <= (ocgy + och))){
            this.attackHitEvent(AD, dir);
            isAttackHitOpponent = true;
        }
        if(attack.weaponName === "rocket"){
            if(((agx + aw) >= cgx) && (agx <= (cgx + cw)) && ((agy + ah) >= cgy) && (agy <= (cgy + ch))){
                this.opponent.attackHitEvent(AD, dir);
                isAttackHitMyself = true;
            }
        }

        if(isAttackHitOpponent || isAttackHitMyself){
            return true;
        }
        return false;
    }
    

    attackHitEvent(AD, dir){
        this.attackHitEventMovedBack(dir);
        this.attackHitEventDamaged(AD);
    }

    attackHitEventDamaged(AD){
        this.opponent.character.stats.damaged(AD);
        if(this.opponent.character.stats.inventory.curWeapon.name === "shield"){
            this.opponent.character.stats.inventory.curWeapon.curBullet = this.oppoCharacter.stats.inventory.curWeapon.curBullet - (AD/2);
            this.opponent.character.stats.inventory.showInventory();
            this.oppoCharacter.showNewWeapon();
            this.opponent.character.showNewWeapon();
            this.showShieldEvent();
        }
        
    }

    showShieldEvent(){
        var img = this.opponent.character.stats.inventory.img;
        this.opponent.canvas.context.drawImage(img, 0, 0, 100, 100, this.opponent.character.dx - 10, this.opponent.character.dy - 10, 100, 100);
        this.canvas.context.drawImage(img, 0, 0, 100, 100, this.oppoCharacter.dx - 10, this.oppoCharacter.dy - 10, 100, 100);
    }

    attacksMovedBack(x, y){
        for(var i = 0; i < this.opponent.attacks.length; i++){
            this.opponent.attacks[i].moveNum(x, y);
        }
    }

    itemsMovedBack(x, y){
        for(var i = 0; i < this.opponent.items.length; i++){
            this.opponent.items[i].x += x;
            this.opponent.items[i].y += y;
        }
    }

    attackHitEventMovedBack(dir){
        if(this.oppoCharacter.stats.inventory.curWeapon.name === "shield"){
            return;
        }
        var bx = this.opponent.baseBackground.x;
        var by = this.opponent.baseBackground.y;
        var gx = this.opponent.character.gx;
        var gy = this.opponent.character.gy;
        var cx = this.opponent.character.dx;
        var cy = this.opponent.character.dy;
        var ax = 0; //attack x
        var ay = 0; //attack y

        if(dir === "left"){
            bx -= 40;
            cx -= 40;
            gx -= 40;
            ax = 40;
        }
        else if(dir === "right"){
            bx += 40;
            cx += 40;
            gx += 40;
            ax = -40;
        }
        else if(dir === "up"){
            by -= 40;
            cy -= 40;
            gy -= 40;
            ay = 40;
        }
        else if(dir === "down"){
            by += 40;
            cy += 40;
            gy += 40;
            ay = -40;
        }

        if(!this.characterAndWallNotCollied(gx, gy)){
            var tempColliedValid = true
            var tempOppoGx = this.opponent.character.gx;
            var tempOppoGy = this.opponent.character.gy;
            var tempOppoCharDx = this.opponent.character.dx;
            var tempOppoCharDy = this.opponent.character.dy;
            while(tempColliedValid){
                if(dir === "left"){
                    tempOppoGx--;
                    tempOppoCharDx--;
                    // opponent attack movedBack event required.
                }
                else if(dir === "right"){
                    tempOppoGx++;
                    tempOppoCharDx++;
                }
                else if(dir === "up"){
                    tempOppoGy--;
                    tempOppoCharDy--;
                }
                else if(dir === "down"){
                    tempOppoGy++;
                    tempOppoCharDy++;
                }
                if(this.characterAndWallNotCollied(tempOppoGx, tempOppoGy)){
                    this.opponent.character.gx = tempOppoGx;
                    this.opponent.character.gy = tempOppoGy;
                    this.opponent.character.dx = tempOppoCharDx;
                    this.opponent.character.dy = tempOppoCharDy;
                }
                else{
                    tempColliedValid = false;
                }
            }
            return;
        }

        if(!this.isBackgroundInsideCanvas(bx, by)){
            if(!this.isCharacterInsideCanvas(cx, cy)){
                return;
            }
            else{
                this.opponent.character.dx = cx;
                this.opponent.character.dy = cy;
            }
        }
        else{
            this.opponent.baseBackground.x = bx;
            this.opponent.baseBackground.y = by;
            this.attacksMovedBack(ax, ay);
            this.itemsMovedBack(ax, ay);
        }
        this.opponent.character.gx = gx;
        this.opponent.character.gy = gy;
    }



    attackEvent(name){
        this.character.stats.updateInventory();
        this.character.showNewWeapon();
        var x = this.character.dx;
        var y = this.character.dy;
        var gx = this.character.dx + this.baseBackground.x;
        var gy = this.character.dy + this.baseBackground.y;
        if(name === "shotgun" || name === "bigShotgun"){
            var rx = Math.floor(Math.random() * 10 - 5);
            var ry = Math.floor(Math.random() * 10 - 5);
            this.attacks.push(new Attack(x + rx, y + ry, gx + rx, gy + ry, this.character.view, this.character.stats.inventory.curWeapon));
        }
        else if(name === "UZI"){
            var rx = Math.floor(Math.random() * 10 - 5);
            var ry = Math.floor(Math.random() * 10 - 5);
            this.attacks.push(new Attack(x + rx, y + ry, gx + rx, gy + ry, this.character.view, this.character.stats.inventory.curWeapon));
        }
        else if(name === "rocket"){
            this.attacks.push(new Attack(x, y, gx, gy, this.character.view, this.character.stats.inventory.curWeapon));
            this.opponent.attackHitEventMovedBack(this.character.getOppositeView());
        }
        else if(name === "shield"){
            //nothing
        }
        else{
            this.attacks.push(new Attack(x, y, gx, gy, this.character.view, this.character.stats.inventory.curWeapon));
        }
        this.character.stats.updateInventory();
        this.character.stats.inventory.showInventory();
        
    }

    attacksPositionSetting(x, y){
        x = x * this.character.stats.moveSpeed;
        y = y * this.character.stats.moveSpeed;
        for(var i = 0; i < this.attacks.length; i++){
            this.attacks[i].x += x;
            this.attacks[i].y += y;
        }
    }

    isAttackInsideMap(x, y, w, h){
        if(x > 0 && (x + w) < this.baseBackground.WIDTH && y > 0 && (y + h) < this.baseBackground.HEIGHT){
            return true;
        }
        return false;
    }

    isAttackInsideRange(sx, sy, x, y, range){
        if(((sx - x)**2 < range**2) && ((sy - y)**2 < range**2)){
            return true;
        }
        return false;
    }

    isAttackValid(attack, dir){
        var sx = attack.sx;
        var sy = attack.sy;
        var x = attack.gx;
        var y = attack.gy;
        var w = attack.aw;
        var h = attack.ah;
        if(this.isAttackInsideMap(x, y, w, h) && this.isAttackInsideRange(sx, sy, x, y, attack.range) && this.isAttackNotCollied(x, y, w, h)){
            if(this.isAttackHit(attack, dir)){
                return false;
            }
            return true;
        }
        return false;
    }

    showAttacks(){
        for(var i = 0; i < this.attacks.length; i++){
            var curAttack = this.attacks[i];   
            if(this.isAttackValid(curAttack, curAttack.direction)){
                curAttack.move();
                if(curAttack.weaponName === "claymore"){
                    this.canvas.context.drawImage(curAttack.img, 0, 0, curAttack.aw, curAttack.ah, curAttack.x, curAttack.y, curAttack.aw, curAttack.ah)
                }
                else if(curAttack.weaponName === "rocket"){
                    this.canvas.context.drawImage(curAttack.img, 0, 0, curAttack.aw, curAttack.ah, curAttack.x, curAttack.y, curAttack.aw, curAttack.ah);
                }
                else{
                    this.canvas.context.fillStyle = "black";
                    this.canvas.context.fillRect(curAttack.x, curAttack.y, curAttack.aw, curAttack.ah); 
                }
            }
            else{ // if attack hits something
                if(curAttack.weaponName === "rocket"){
                    var tempAttack; 
                    for(var j = 0; j < 10; j++){
                        tempAttack = JSON.parse(JSON.stringify(curAttack));
                        var rNumX = Math.floor(Math.random() * 200 - 100);
                        var rNumY = Math.floor(Math.random() * 200 - 100);
                        tempAttack.aw = 40;
                        tempAttack.ah = 40;
                        tempAttack.x = tempAttack.x + rNumX;
                        tempAttack.y = tempAttack.y + rNumY;
                        tempAttack.gx = tempAttack.gx + rNumX;
                        tempAttack.gy = tempAttack.gy + rNumY;
                        var oppoX = tempAttack.gx - this.opponent.baseBackground.x;
                        var oppoY = tempAttack.gy - this.opponent.baseBackground.y;
                        //this.canvas.context.fillRect(tempAttack.x, tempAttack.y, tempAttack.aw, tempAttack.ah);
                        this.canvas.context.drawImage(curAttack.hitImg, 0, 0, 120, 120, tempAttack.x, tempAttack.y, 120, 120);
                        this.opponent.canvas.context.drawImage(curAttack.hitImg, 0, 0, 120, 120, oppoX, oppoY, 120, 120); //ìƒëŒ€ìª½ ìžì‹  ê³µê²© ì´íŒ©íŠ¸
                        this.isAttackHit(tempAttack, tempAttack.direction);
                    }
                }
                this.attacks.splice(i, 1);
            }
        }
    }

    showOppoAttacks(){
        for(var i = 0; i < this.oppoAttacks.length; i++){
            var curAttack = this.oppoAttacks[i];
            var tempX = curAttack.gx - this.baseBackground.x;
            var tempY = curAttack.gy - this.baseBackground.y;
            if(curAttack.weaponName === "claymore"){
                this.canvas.context.drawImage(curAttack.img, 0, 0, curAttack.aw, curAttack.ah, tempX, tempY, curAttack.aw, curAttack.ah);
            }
            else if(curAttack.weaponName === "rocket"){
                this.canvas.context.drawImage(curAttack.img, 0, 0, curAttack.aw, curAttack.ah, tempX, tempY, curAttack.aw, curAttack.ah);
            }
            else{
                this.canvas.context.fillStyle = "black";
                this.canvas.context.fillRect(tempX, tempY, curAttack.aw, curAttack.ah);
            }
        }
    }

    attacksReposition(){
        for(var i = 0; i < this.attacks.length; i++){
            this.attacks[i].reposition(this.baseBackground.x, this.baseBackground.y);
        }
    }

    getItem(item){
        if(item.type === "weapon"){
            this.character.stats.inventory.addWeapon(item.name);
        }
        else if(item.type === "helmet"){
            this.character.stats.inventory.addHelmet(item.name);
        }
        else if(item.type === "armor"){
            this.character.stats.inventory.addArmor(item.name);
        }
        else if(item.type === "shoes"){
            this.character.stats.inventory.addShoes(item.name);
        }
        this.character.stats.updateInventory();
    }

    showItems(){
        for(var i = 0; i < this.items.length; i++){
            var curItem = this.items[i];
            this.canvas.context.drawImage(curItem.image, 0, 0, curItem.width, curItem.height, curItem.x, curItem.y, curItem.width, curItem.height);
            if(this.isItemCollied(curItem)){
                this.getItem(curItem);
                this.character.stats.gainHealthPoint(50);
                this.items.splice(i, 1);
                this.opponent.items.splice(i, 1);
                this.character.stats.inventory.showInventory();
                
            } 
        }

    }

    isCharacterDead(){
        if(this.playerState){
            if(!this.character.stats.state){
                this.lifeDecrease();
                this.character.dead();
                this.opponent.oppoCharacter.dead();
                this.playerState = false;
                return true;
            }
            return false;
        }
    }

    showBaseBackground(){
        this.canvas.context.drawImage(
            this.baseBackground.image, 
            this.baseBackground.x, 
            this.baseBackground.y, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT, 
            0, 
            0, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT
        );
    }

    showMiddleBackground(){
        this.canvas.context.drawImage(
            this.middleBackground.image, 
            this.baseBackground.x, 
            this.baseBackground.y, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT, 
            0, 
            0, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT
        );
    }

    showTopBackground(){
        this.canvas.context.drawImage(
            this.topBackground.image, 
            this.baseBackground.x, 
            this.baseBackground.y, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT, 
            0, 
            0, 
            this.canvas.WIDTH, 
            this.canvas.HEIGHT
        );
    }

    showCharacter(){
        this.canvas.context.drawImage
        (this.character.image,
         this.character.sx, 
         this.character.sy, 
         this.character.sw, 
         this.character.sh, 
         this.character.dx, 
         this.character.dy, 
         this.character.dw, 
         this.character.dh);
        
        this.canvas.context.drawImage
        (this.character.stats.inventory.curHelmet.image,
         this.character.sx, 
         this.character.sy, 
         this.character.sw, 
         this.character.sh, 
         this.character.dx, 
         this.character.dy, 
         this.character.dw, 
         this.character.dh
        );

        this.canvas.context.drawImage
        (this.character.stats.inventory.curArmor.image,
         this.character.sx, 
         this.character.sy, 
         this.character.sw, 
         this.character.sh, 
         this.character.dx, 
         this.character.dy, 
         this.character.dw, 
         this.character.dh
        );
    }

    showOppoCharacter(){
        this.canvas.context.drawImage
        (this.oppoCharacter.image,
         this.oppoCharacter.sx, 
         this.oppoCharacter.sy, 
         this.oppoCharacter.sw, 
         this.oppoCharacter.sh, 
         this.oppoCharacter.dx, 
         this.oppoCharacter.dy, 
         this.oppoCharacter.dw, 
         this.oppoCharacter.dh);
    

        this.canvas.context.drawImage
        (this.oppoCharacter.stats.inventory.curHelmet.image,
         this.oppoCharacter.sx, 
         this.oppoCharacter.sy, 
         this.oppoCharacter.sw, 
         this.oppoCharacter.sh, 
         this.oppoCharacter.dx, 
         this.oppoCharacter.dy, 
         this.oppoCharacter.dw, 
         this.oppoCharacter.dh
        );

        this.canvas.context.drawImage
        (this.oppoCharacter.stats.inventory.curArmor.image,
         this.oppoCharacter.sx, 
         this.oppoCharacter.sy, 
         this.oppoCharacter.sw, 
         this.oppoCharacter.sh, 
         this.oppoCharacter.dx, 
         this.oppoCharacter.dy, 
         this.oppoCharacter.dw, 
         this.oppoCharacter.dh
        );
    }

    showTexts(){
        var txt = this.character.stats.inventory.text.getText();
        this.canvas.context.font = "18px Arial";
        this.canvas.context.fillStyle = "blue";
        this.canvas.context.textAlign = "center";
        this.canvas.context.fillText(txt, 400, 570);
    }

    showInfo(){
        this.character.stats.showInfo();
    }
}


