var KEY1 = {
    //player one
    LEFT : 65,
    UP : 87,
    RIGHT : 68,
    DOWN : 83,
    ATTACK : 50,
    CHANGE : 49,
    TRASH : 85
}

var KEY2 = {
    //player two
    LEFT : 37,
    UP : 38,
    RIGHT : 39,
    DOWN : 40,
    ATTACK : 222,
    CHANGE : 80,
    TRASH : 36
}

var keyOneMap = {
    [KEY1.LEFT] : false,
    [KEY1.UP] : false,
    [KEY1.RIGHT] : false,
    [KEY1.DOWN] : false,
    [KEY1.ATTACK] : false,
    [KEY1.CHANGE] : false,
    [KEY1.TRASH] : false
}

var keyTwoMap = {
    [KEY2.LEFT] : false,
    [KEY2.UP] : false,
    [KEY2.RIGHT] : false,
    [KEY2.DOWN] : false,
    [KEY2.ATTACK] : false,
    [KEY2.CHANGE] : false,
    [KEY2.TRASH] : false
}

function renderingCharacters(){
    player2.oppoCharacter.gx = player1.character.gx;
    player2.oppoCharacter.gy = player1.character.gy;
    player2.oppoCharacter.sx = player1.character.sx;
    player2.oppoCharacter.sy = player1.character.sy;
    player2.oppoCharacter.dx = player1.character.gx - player2.baseBackground.x;
    player2.oppoCharacter.dy = player1.character.gy - player2.baseBackground.y;
    player2.oppoCharacter.viewUpdate(player1.character.view);
    player2.oppoCharacter.isMoving = player1.character.isMoving;
    player2.oppoAttacks = player1.attacks;
    player2.oppoCharacter.stats = player1.character.stats;
    

    player1.oppoCharacter.gx = player2.character.gx;
    player1.oppoCharacter.gy = player2.character.gy;
    player1.oppoCharacter.dx = player2.character.gx - player1.baseBackground.x;
    player1.oppoCharacter.dy = player2.character.gy - player1.baseBackground.y;
    player1.oppoCharacter.viewUpdate(player2.character.view);
    player1.oppoCharacter.isMoving = player2.character.isMoving;
    player1.oppoAttacks = player2.attacks;
    player1.oppoCharacter.stats = player2.character.stats;
}


function moveCharacter(player){
    var tempCx = player.character.dx;
    var tempCy = player.character.dy;
    var tempBx = player.baseBackground.x;
    var tempBy = player.baseBackground.y;
    var tempGx = player.character.gx;
    var tempGy = player.character.gy;
    var moveSpeed = player.character.stats.moveSpeed;
    if(player.name === "A"){
        var keyStateLeft = keyOneMap[KEY1.LEFT];
        var keyStateRight = keyOneMap[KEY1.RIGHT];
        var keyStateUp = keyOneMap[KEY1.UP];
        var keyStateDown = keyOneMap[KEY1.DOWN];
    }
    else{
        var keyStateLeft = keyTwoMap[KEY2.LEFT];
        var keyStateRight = keyTwoMap[KEY2.RIGHT];
        var keyStateUp = keyTwoMap[KEY2.UP];
        var keyStateDown = keyTwoMap[KEY2.DOWN];
    }

    if(!keyStateLeft && !keyStateRight && !keyStateUp && !keyStateDown){
        player.character.isMoving = false;
        return;
    }
    if(keyStateLeft){
        tempCx = tempCx - moveSpeed;
        tempBx = tempBx - moveSpeed;
        tempGx = tempGx - moveSpeed;
        if(!keyStateRight && !keyStateUp && !keyStateDown){
            player.character.viewUpdate("left");
        }
    }
    if(keyStateUp){
        tempCy = tempCy - moveSpeed;
        tempBy = tempBy - moveSpeed;
        tempGy = tempGy - moveSpeed;
        if(!keyStateRight && !keyStateLeft && !keyStateDown){
            player.character.viewUpdate("up");
        }
    }
    if(keyStateRight){
        tempCx = tempCx + moveSpeed;
        tempBx = tempBx + moveSpeed;
        tempGx = tempGx + moveSpeed;
        if(!keyStateLeft && !keyStateUp && !keyStateDown){
            player.character.viewUpdate("right");
        }
    }
    if(keyStateDown){
        tempCy = tempCy + moveSpeed;
        tempBy = tempBy + moveSpeed;
        tempGy = tempGy + moveSpeed;
        if(!keyStateRight && !keyStateUp && !keyStateLeft){
            player.character.viewUpdate("down");
        }
    }

    if(player.isValid(tempCx, tempCy, tempBx, tempBy, tempGx, tempGy)){
        
    }
    else{
        player.character.isMoving = false;
        return;

    }

    setTimeout(function(){
        moveCharacter(player);
    }, 5);
}

function attack(player){
    player.character.showNewWeapon();
    player.opponent.oppoCharacter.showNewWeapon();
    var keyState;
    if(player.name === "A"){
        keyState = keyOneMap[KEY1.ATTACK];
    }
    else{
        keyState = keyTwoMap[KEY2.ATTACK];
    }
    
    var curWeaponName = player.character.stats.inventory.curWeapon.name;
    if(curWeaponName === "UZI" && keyState){
        player.attackEvent("UZI");
        setTimeout(function(){
            attack(player);
        }, 10);
    }
    else if(curWeaponName === "shotgun"){
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
    }
    else if(curWeaponName === "bigShotgun"){
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
        player.attackEvent("shotgun");
    }
    else if(curWeaponName === "rocket"){
        player.attackEvent("rocket");
    }
    else if(curWeaponName === "shield"){
        player.attackEvent("shield");
    }
    else{
        player.attackEvent();
        return;
    }
}

function weaponChange(player){
    player.character.stats.inventory.changeWeapon();
    this.renderingCharacters();
    player.character.showNewWeapon();
    player.opponent.oppoCharacter.showNewWeapon();
}

function weaponTrash(player){
    player.character.stats.inventory.removeWeapon();
    this.renderingCharacters();
    player.character.showNewWeapon();
    player.opponent.oppoCharacter.showNewWeapon();
}

function addEventListener(){
    document.addEventListener('keydown', event => {
        console.log(event.keyCode);
        if(event.keyCode in keyOneMap){
            if(player1.character.stats.state){
                if(event.keyCode === KEY1.TRASH && !keyOneMap[KEY1.TRASH]){
                    keyOneMap[KEY1.TRASH] = true;
                    weaponTrash(player1);
                }

                keyOneMap[event.keyCode] = true;
                if(event.keyCode === KEY1.ATTACK && !player1.character.isAttacking){
                    player1.character.isAttacking = true;
                    attack(player1);
                }
                if(event.keyCode === KEY1.CHANGE){
                    weaponChange(player1);
                }
                if(!player1.character.isMoving){
                    player1.character.isMoving = true;
                    moveCharacter(player1);
                }
            }
        }
        else if(event.keyCode in keyTwoMap){
            if(player2.character.stats.state){
                if(event.keyCode === KEY2.TRASH && !keyTwoMap[KEY2.TRASH]){
                    keyTwoMap[KEY2.TRASH] = true;
                    weaponTrash(player2);
                }
                keyTwoMap[event.keyCode] = true;
                if(event.keyCode === KEY2.ATTACK && !player2.character.isAttacking){
                    player2.character.isAttacking = true;
                    attack(player2);
                }
                if(event.keyCode === KEY2.CHANGE){
                    weaponChange(player2);
                }
                if(!player2.character.isMoving){
                    player2.character.isMoving = true;
                    moveCharacter(player2);
                }
            }
        }

    })
    document.addEventListener('keyup', event => {
        if(event.keyCode in keyOneMap){
            keyOneMap[event.keyCode] = false;
            if(event.keyCode === KEY1.ATTACK){
                player1.character.isAttacking = false;
            }
        }

        else if(event.keyCode in keyTwoMap){
            keyTwoMap[event.keyCode] = false;
            if(event.keyCode === KEY2.ATTACK){
                player2.character.isAttacking = false;
            }
        }
    })
}