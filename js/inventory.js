class Inventory{
    name;
    type;
    curWeapon;
    curHelmet;
    curShoes;
    weapons;
    text;
    img;

    constructor(name, type){
        this.name = name;
        this.type = type;
        this.curWeapon = new Weapon("pistol");
        this.curHelmet = new Helmet("none");
        this.curArmor = new Armor("none");
        this.curShoes = new Shoes("none");
        this.weapons = new Array(3);
        this.weapons[0] = new Weapon("none");
        this.weapons[1] = new Weapon("none");
        this.weapons[2] = new Weapon("none");
        this.text = new Text();
        if(type !== "oppo"){
            this.showInventory();
        }
        this.img = new Image();
    }

    changeWeapon(){
        var tempWeapon = JSON.parse(JSON.stringify(this.curWeapon));
        this.curWeapon = this.weapons.shift();
        this.weapons.push(tempWeapon);
        if(this.curWeapon.name === "none"){
            this.changeWeapon();
        }
        else if(this.curWeapon.name === "shield"){
            this.img.src = 'css/images/weapons/shieldOn.png';
        }
        this.showInventory();
        
    }

    alreadyHaveWeapon(name){
        if(name === this.curWeapon.name){
            this.curWeapon.curBullet = this.curWeapon.fullBullet;
            return true;
        }
        for(var i = 0; i < this.weapons.length; i++){
            if(name === this.weapons[i].name){
                this.weapons[i].curBullet = this.weapons[i].fullBullet;
                return true;
            }
        }
        return false;
    }

    addWeapon(name){
        for(var i = 0; i < this.weapons.length; i++){
            if(!this.alreadyHaveWeapon(name)){
                if(this.weapons[i].name === "none"){
                    this.weapons[i] = new Weapon(name);
                }
            }
        }
        this.text.addText(name);
    }
    addHelmet(name){
        var newHelmet = new Helmet(name);
        if(this.curHelmet.defense < newHelmet.defense){
            this.curHelmet = newHelmet;
        }
        this.text.addText(name + "-helmet");
    }
    addArmor(name){
        var newArmor = new Armor(name);
        if(this.curArmor.defense < newArmor.defense){
            this.curArmor = newArmor;
        }
        this.text.addText(name + "-armor");
    }
    addShoes(name){
        var newShoes = new Shoes(name);
        if(this.curShoes.defense < newShoes.defense){
            this.curShoes = newShoes;
        }
        this.text.addText(name + "-shoes");
    }
    removeWeapon(){
        if(this.curWeapon.name !== "pistol"){
            this.curWeapon = new Weapon("none");
            this.changeWeapon();
        }
    }

    showInventory(){
        if(this.curWeapon.name !== "pistol" && this.curWeapon.curBullet <= 0){
            this.removeWeapon();
        }
        var weapon1 = document.getElementById("weapon1" + this.name);
        var weapon2 = document.getElementById("weapon2" + this.name);
        var weapon3 = document.getElementById("weapon3" + this.name);
        var weapon4 = document.getElementById("weapon4" + this.name);
        var helmet = document.getElementById("helmet" + this.name);
        var armor = document.getElementById("armor" + this.name);
        var shoes = document.getElementById("shoes" + this.name);
        var bullet = document.getElementById("bullet" + this.name);
        var helmetInfo = document.getElementById("h-info" + this.name);
        var armorInfo = document.getElementById("a-info" + this.name);
        var shoesInfo = document.getElementById("s-info" + this.name);
 
        $("#weapon1" + this.name).removeClass().addClass(this.curWeapon.name);
        $("#weapon2" + this.name).removeClass().addClass(this.weapons[0].name);
        $("#weapon3" + this.name).removeClass().addClass(this.weapons[1].name);
        $("#weapon4" + this.name).removeClass().addClass(this.weapons[2].name);
        
        $("#helmet" + this.name).removeClass().addClass(this.curHelmet.name + "-helmet");
        $("#armor" + this.name).removeClass().addClass(this.curArmor.name + "-armor");
        $("#shoes" + this.name).removeClass().addClass(this.curShoes.name + "-shoes");

        weapon1.innerHTML = this.curWeapon.name;
        bullet.innerHTML = this.curWeapon.fullBullet + " / " + this.curWeapon.curBullet;
        helmetInfo.innerHTML = "+" + this.curHelmet.defense + "%";
        armorInfo.innerHTML = "+" + this.curArmor.defense + "%";
        shoesInfo.innerHTML = "+" + this.curShoes.defense + "%";
    }
}

class Weapon{
    name;
    attackDamage;
    fullBullet;
    curBullet;
        
    constructor(name){
        this.name = name;
        if(name === "none"){
            this.attackDamage = 0;
            this.fullBullet = 0;
            this.curBullet = 0;
        }
        else if(name === "pistol"){
            this.attackDamage = 100;
            this.fullBullet = "OO";
            this.curBullet = "OO";
        }
        else if(name === "UZI"){
            this.attackDamage = 5;
            this.fullBullet = 500;
            this.curBullet = 500;
        }
        else if(name === "shotgun"){
            this.attackDamage = 100;
            this.fullBullet = 120;
            this.curBullet = 120;
        }
        else if(name === "rocket"){
            this.attackDamage = 300;
            this.fullBullet = 30;
            this.curBullet = 30;
        }
        else if(name === "claymore"){
            this.attackDamage = 300;
            this.fullBullet = 5;
            this.curBullet = 5;
        }
        else if(name ==="bigShotgun"){
            this.attackDamage = 150;
            this.fullBullet = 200;
            this.curBullet = 200;
        }
        else if(name === "shield"){
            this.attackDamage = 0;
            this.fullBullet = 2000;
            this.curBullet = 2000;
            
        }
    }
}

class Helmet{
    name;
    defense;
    image;

    constructor(name){
        this.name = name;
        this.image = new Image();
        this.image.src = 'css/images/characters/' + this.name + '-helmet.png';
        if(name === "none"){
            this.defense = 0;
        }
        else if(name === "leather"){
            this.defense = 5;
        }
        else if(name === "wood"){
            this.defense = 10;
        }
        else if(name === "steel"){
            this.defense = 30;
        }
    }
}

class Armor{
    name;
    defense;
    image;

    constructor(name){
        this.name = name;
        this.image = new Image();
        this.image.src = 'css/images/characters/' + this.name + '-armor.png';
        if(name === "none"){
            this.defense = 0;
        }
        else if(name === "leather"){
            this.defense = 15;
        }
        else if(name === "wood"){
            this.defense = 20;
        }
        else if(name === "steel"){
            this.defense = 40;
        }
    }
}

class Shoes{
    name;
    defense;
    moveSpeed;

    constructor(name){
        this.name = name;
        if(name === "none"){
            this.defense = 0;
            this.moveSpeed = 1;
        }
        else if(name === "leather"){
            this.defense = 5;
            this.moveSpeed = 1.2;
        }
        else if(name === "wood"){
            this.defense = 7;
            this.moveSpeed = 1.2;
        }
        else if(name === "steel"){
            this.defense = 20;
            this.moveSpeed = 1.5;
        }
    }
}

