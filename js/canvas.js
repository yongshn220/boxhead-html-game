class Canvas{
    x;
    y;
    WIDTH;
    HEIGHT;
    element;
    context;
    
    constructor(name){
      this.x = 0;
      this.y = 0;
      this.WIDTH = 800;
      this.HEIGHT = 600;
      this.element = document.getElementById('canvas' + name);
      this.element.width = this.WIDTH;
      this.element.height = this.HEIGHT;
      this.context = this.element.getContext('2d');
    }

    clearCanvas(){
        this.context.clearRect(0, 0, 800, 600);
    }

}