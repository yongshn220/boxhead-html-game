class Item{
    name;
    x;
    y;
    gx;
    gy;
    width;
    height;
    image;
    type;

    constructor(x, y, gx, gy, num){
        this.x = x;
        this.y = y;
        this.gx = gx;
        this.gy = gy;
        this.width = 40;
        this.height = 40;
        this.image = new Image();
        this.setName(num);
        
    }

    setName(num){
        switch(num){
            case 0: this.name = "claymore"; this.type = "weapon";
                    break;
            case 1: this.name = "shotgun"; this.type = "weapon";
                    break;
            case 2: this.name = "UZI"; this.type = "weapon";
                    break;
            case 3: this.name = "rocket"; this.type = "weapon";
                    break;
            case 4: this.name = "leather"; this.type = "helmet";
                    break;
            case 5: this.name = "leather"; this.type = "armor";
                    break;
            case 6: this.name = "leather"; this.type = "shoes";
                    break;
            case 7: this.name = "wood"; this.type = "helmet";
                    break;
            case 8: this.name = "wood"; this.type = "armor";
                    break;
            case 9: this.name = "wood"; this.type = "shoes";
                    break;
            case 10: this. name = "steel"; this.type = "helmet";
                    break;
            case 11: this.name = "steel"; this.type = "armor";
                    break;
            case 12: this.name = "steel"; this.type = "shoes";
                    break;
            case 13: this.name = "bigShotgun"; this.type = "weapon";
                    break;        
            case 14: this.name = "shield"; this.type = "weapon";
                    break;
        }
        this.setImage();
    }

    setImage(){
        if(this.type === "weapon"){
            this.image.src = 'css/images/item-weapon.png';
        }
        else{
            this.image.src = 'css/images/item-armor.png';
        }
    }

    reposition(bx, by){
        this.x = this.gx - bx;
        this.y = this.gy - by;
    }

    move(x, y){
        this.x = this.x + x;
        this.y = this.y + y;
    }
}