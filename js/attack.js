class Attack{
    sx;              // start position x    
    sy;              // start position y
    x;               //x position in canvas
    y;
    gx;              // x position in map
    gy;
    aw;              // attack width size
    ah;              // attack height size
    speed;
    acc;             // acceleration
    direction;
    range;
    weaponName;
    shotDirection;
    attackDamage;
    img;
    hitImg;

    constructor(x, y, gx, gy, direction, weapon){
        
        this.x = x;
        this.y = y;
        this.gx = gx;
        this.gy = gy;
        this.speed = 10;
        this.acc = 1;
        this.range = 0;
        this.direction = direction;
        this.img = new Image();
        this.hitImg = new Image();
        this.weaponName = weapon.name;
        this.attackPositionAdjust();
        this.setAttackInfo();
        this.attackDamage = weapon.attackDamage;
        if(this.weaponName !== "pistol"){
            weapon.curBullet--;
        }
    }

    attackPositionAdjust(){
        if(this.direction === "left"){
            this.x = this.x - 15;
            this.y = this.y + 30;
            this.gx = this.gx - 15;
            this.gy = this.gy + 30;

        }
        else if(this.direction === "up"){
            this.x = this.x + 56;
            this.gx = this.gx + 56;
        }
        else if(this.direction === "right"){
            this.x = this.x + 80;
            this.y = this.y + 40;
            this.gx = this.gx + 80;
            this.gy = this.gy + 40;
        }
        else if(this.direction === "down"){
            this.x = this.x + 20;
            this.y = this.y + 85;
            this.gx = this.gx + 20;
            this.gy = this.gy + 85;
        }
        this.sx = this.gx;
        this.sy = this.gy;
    }

    move(){
        this.speed = this.speed * this.acc;
        if(this.direction === "left"){
            this.x = this.x - this.speed;
            this.gx = this.gx - this.speed;
        }
        else if(this.direction === "up"){
            this.y = this.y - this.speed;
            this.gy = this.gy - this.speed;
        }
        else if(this.direction === "right"){
            this.x = this.x + this.speed;
            this.gx = this.gx + this.speed;
        }
        else if(this.direction === "down"){
            this.y = this.y + this.speed;
            this.gy = this.gy + this.speed;
        }
        if(this.weaponName === "shotgun" || this.weaponName === "bigShotgun"){
            this.x = this.x + this.shotDirection;
            this.gx = this.gx + this.shotDirection;
            this.y = this.y + this.shotDirection;
            this.gy = this.gy + this.shotDirection;
        }
    }

    moveNum(x, y){
        this.x = this.x + x;
        this.y = this.y + y;
    }

    setShotgunEffect(n){
        this.shotDirection = Math.floor(Math.random() * 3 - 1);
        this.shotDirection *= n;
    }
    

    setAttackInfo(){
        if(this.direction === "left" || this.direction === "right"){
            if(this.weaponName === "pistol"){
                this.aw = 8;
                this.ah = 8;
                this.speed = 15;
                this.acc = 1;
                this.range = 350;
            }
            else if(this.weaponName === "UZI"){
                this.aw = 20;
                this.ah = 1;
                this.speed = 30;
                this.acc = 1;
                this.range = 2000;
            }
            else if(this.weaponName === "shotgun"){
                this.aw = 50;
                this.ah = 3;
                this.speed = 50;
                this.acc = 1;
                this.range = 300;
                this.setShotgunEffect(4);
            }
            else if(this.weaponName === "rocket"){
                this.aw = 40;
                this.ah = 20;
                this.speed = 5;
                this.acc = 1.04;
                this.range = 2000;
                this.img.src = 'css/images/weapons/rocketOn-' + this.direction + '.png';
                this.hitImg.src = 'css/images/weapons/rocketHit.png';
                this.rocketSetting();
            }
            else if(this.weaponName === "claymore"){
                this.aw = 20;
                this.ah = 20;
                this.speed = 0;
                this.range = 100;
                this.img.src = 'css/images/weapons/claymoreOn.png';
            }
            else if(this.weaponName === "bigShotgun"){
                this.aw = 70;
                this.ah = 3;
                this.speed = 70;
                this.acc = 1;
                this.range = 800;
                this.setShotgunEffect(8);
            }
            else if(this.weaponName === "shield"){
                this.aw = 0;
                this.ah = 0;
                this.speed = 0;
                this.acc = 0;
                this.range = 0;
            }
        }
        else if(this.direction === "up" || this.direction === "down"){
            if(this.weaponName === "pistol"){
                this.aw  = 8;
                this.ah = 8;
                this.speed = 15;
                this.acc = 1;
                this.range = 350;
            }
            else if(this.weaponName === "UZI"){
                this.aw = 1;
                this.ah = 20;
                this.speed = 30;
                this.acc = 1;
                this.range = 2000;
            }
            else if(this.weaponName === "shotgun"){
                this.aw = 3;
                this.ah = 50;
                this.speed = 50;
                this.acc = 1;
                this.range = 200;
                this.setShotgunEffect(4);
            }
            else if(this.weaponName === "rocket"){
                this.aw = 20;
                this.ah = 40;
                this.speed = 5;
                this.acc = 1.04;
                this.range = 2000;
                this.img.src = 'css/images/weapons/rocketOn-' + this.direction + '.png';
                this.hitImg.src = 'css/images/weapons/rocketHit.png';
                this.rocketSetting();
            }
            else if(this.weaponName === "claymore"){
                this.aw = 20;
                this.ah = 20;
                this.speed = 0;
                this.range = 100;
                this.img.src = 'css/images/weapons/claymoreOn.png';
            }
            else if(this.weaponName === "bigShotgun"){
                this.aw = 3;
                this.ah = 70;
                this.speed = 70;
                this.acc = 1;
                this.range = 800;
                this.setShotgunEffect(8);
            }
            else if(this.weaponName === "shield"){
                this.aw = 0;
                this.ah = 0;
                this.speed = 0;
                this.acc = 0;
                this.range = 0;
            }
        }
        //this.speed = 0;
    }

    rocketSetting(){
        if(this.direction === "left"){
            this.x = this.x - 20;
            this.gx = this.gx - 20;
        }
        else if(this.direction === "up"){
            this.y = this.y - 20;
            this.gy = this.gy - 20;
        }
        else if(this.direction === "right"){
            this.x = this.x + 20;
            this.gx = this.gx + 20;
        }
        else if(this.direction === "down"){

        }
    }

    reposition(bx, by){
        this.x = this.gx - bx;
        this.y = this.gy - by;
    }
}