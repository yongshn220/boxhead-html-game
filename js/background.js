class Background{
    image;
    x;
    y;
    WIDTH;
    HEIGHT;
    wallMap;

    constructor(src){
        this.x = 0;
        this.y = 0;
        this.WIDTH = 2000;
        this.HEIGHT = 2000;
        this.image = new Image();
        this.image.src = src;
        this.wallMap = Array.from({length : 50}, () => Array(50).fill(false));
        this.wallMapSetting();
    }

    changeImageSource(src){
        this.image.src = src;
    }

    move(direction){
        if(direction === "right"){
            this.x = this.x + 1;
        }
        else if(direction === "left"){
            this.x = this.x - 1;
        }
        else if(direction === "up"){
            this.y = this.y - 1;
        }
        else if(direction === "down"){
            this.y = this.y + 1;
        }
    }

    wallMapSetting(){
       
        
        for(var y = 0; y < 7; y++){
            for(var x = 11; x < 15; x++){
                this.wallMap[y][x] = true;
            }
        }
        
        for(var y = 2; y < 5; y++){
            for(var x = 17; x < 28; x++){
                this.wallMap[y][x] = true;
            }
            for(var x = 30; x < 41; x++){
                this.wallMap[y][x] = true;
            }
            for(var x = 43; x < 47; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 5; y < 7; y++){
            for(var x = 17; x < 20; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 5; y < 13; y++){
            for(var x = 45; x < 47; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 7; y < 10; y++){
            for(var x = 1; x < 9; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 10; y < 14; y++){
            for(var x = 17; x < 43; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 12; y < 45; y++){
            this.wallMap[y][3] = true;
        }

        for(var y = 12; y < 14; y++){
            for(var x = 6; x < 15; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 14; y < 19; y++){
            for(var x = 17; x < 20; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 16; y < 19; y++){
            for(var x = 6; x < 17; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 17; y < 32; y++){
            for(var x = 39; x < 43; x++){
                this.wallMap[y][x] = true;
            }

            for(var x = 48; x < 50; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 22; y < 26; y++){
            for(var x = 4; x < 17; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 28; y < 42; y++){
            for(var x = 7; x < 10; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 28; y < 32; y++){
            for(var x = 13; x < 39; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 32; y < 41; y++){
            for(var x = 23; x < 27; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 33; y < 36; y++){
            for(var x = 10; x < 15; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 34; y < 38; y++){
            for(var x = 41; x < 49; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 37; y < 41; y++){
            for(var x = 27; x < 36; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 38; y < 44; y++){
            for(var x = 12; x < 15; x++){
                this.wallMap[y][x] = true;
            }
        }

        for(var y = 41; y < 49; y++){
            for(var x = 38; x < 41; x++){
                this.wallMap[y][x] = true;
            }
        }
               
        for(var y = 44; y < 47; y++){
            for(var x = 7; x < 18; x++){
                this.wallMap[y][x] = true;
            }
            for(var x = 21; x < 36; x++){
                this.wallMap[y][x] = true;
            }
        }
        
    }
}