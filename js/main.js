var requestId;
var player1;
var player2;
var char1 = 'css/images/characters/charA-pistol.png';
var char2 = 'css/images/characters/charB-pistol.png';
$(document).ready(function(){

    player1 = new Player('A', char1, char2);
    player2 = new Player('B', char2, char1);

    player1.opponent = player2;
    player2.opponent = player1;
    animate();
    addEventListener(); 

})

function newItem(){
    var gx = Math.floor(Math.random() * 2000);
    var gy = Math.floor(Math.random() * 2000);
    var rNum = Math.floor(Math.random() * 100);
    console.log(rNum);
    if(rNum >= 0 && rNum < 20){
        rNum = Math.floor(Math.random() * 3 + 10);
    }
    else if(rNum >= 20 && rNum < 40){
        rNum = Math.floor(Math.random() * 3 + 4);
    }
    else if(rNum >= 40 && rNum < 50){
        rNum = Math.floor(Math.random() * 3 + 7);
    }
    else if(rNum >= 50 && rNum < 75){
        rNum = Math.floor(Math.random() * 3);
    }
    else{
        rNum = 3;
    }

    if(player1.itemAndWallNotCollied(gx, gy) && player2.itemAndWallNotCollied(gx, gy) && player1.items.length < 10){
        player1.newItem(gx, gy, rNum);
        player2.newItem(gx, gy, rNum);
    }
}

function itemOfDeads(deadPlayer){
    var rNum = Math.floor(Math.random() * 5 + 1);
    if(rNum === 4){
        rNum = 13; // bigshotgun
    }
    else if(rNum === 5){
        rNum = 14; // shield    
    }
    deadPlayer.newItem(deadPlayer.character.gx, deadPlayer.character.gy, rNum);
    deadPlayer.opponent.newItem(deadPlayer.character.gx, deadPlayer.character.gy, rNum);
}

function newLife(player, opponent){
    setTimeout(function(){
        player.character.newLife(player.life);
        player.attacksReposition();
        player.itemsPositionReset();
        player.playerState = true;
        opponent.oppoCharacter.newLife();
    }, 3000);
}

function occupationTimePassEvent(){
    player1.occupationTimePass();
    player2.occupationTimePass();
}

timePlayerOne = {
    start : 0,
    elapsed : 0,
    step : 200,
}

itemTime = {
    start : 0,
    elapsed : 0,
    item : 1
}

occupationTime = {
    start : 0,
    elapsed : 0,
    interval : 1000
}

function animate(now = 0){
    timePlayerOne.elapsed = now - timePlayerOne.start;
    itemTime.elapsed = now - itemTime.start;
    occupationTime.elapsed = now - occupationTime.start;

    //walking motion event
    if(timePlayerOne.elapsed > timePlayerOne.step){
        timePlayerOne.start = now;
        playersImageUpdate();
    }
    //item event
    if(itemTime.elapsed > itemTime.item){
        itemTime.start = now;
        if(player1.items.length < 10){
            newItem();
        }
    }

    if(occupationTime.elapsed > occupationTime.interval){
        occupationTime.start = now;
        occupationTimePassEvent();
    }
    
    //new life event
    playersClearCanvas();
    playersAnimation();
    renderingCharacters();
    requestId = requestAnimationFrame(animate);
}


function playersGameStateCheck(){
    if(!player1.gameState || !player2.gameState){
        cancelAnimationFrame(requestId);
    }
}


function playersImageUpdate(){
    player1.character.imageUpdate();
    player1.oppoCharacter.imageUpdate();
    player2.character.imageUpdate();
    player2.oppoCharacter.imageUpdate();
}

function playersAnimation(){
    playersHealthPointCheck();
    playersDrawFirstBackground();

    player1.showAttacks();
    player1.showItems(); 
    player1.showOppoAttacks();
    player1.showCharacter();
    player1.showOppoCharacter();
    player1.showInfo();

    player2.showAttacks();
    player2.showItems();
    player2.showOppoAttacks();
    player2.showCharacter();
    player2.showOppoCharacter();
    player2.showInfo();

    playersDrawLastBackground();
    playersTextMessage();
    playersDrawOccupation();

}

function playersDrawOccupation(){
    player1.characterOccupation();
    player2.characterOccupation();
}


function playersClearCanvas(){
    player1.canvas.clearCanvas();
    player2.canvas.clearCanvas();
}

function playersDrawFirstBackground(){
    player1.showBaseBackground();
    player1.showMiddleBackground();
    player2.showBaseBackground();
    player2.showMiddleBackground();
}

function playersDrawLastBackground(){
    player1.showTopBackground();
    player2.showTopBackground();
}

function playersHealthPointCheck(){
    if(player1.isCharacterDead()){
        itemOfDeads(player1);
        newLife(player1, player2);
    }
    if(player2.isCharacterDead()){
        itemOfDeads(player2);
        newLife(player2, player1);
    }
}

function playersTextMessage(){
    if(!player1.character.stats.inventory.text.isEmpty()){
        player1.showTexts();
        textMessageEvent(player1);
    }
    if(!player2.character.stats.inventory.text.isEmpty()){
        player2.showTexts();
        textMessageEvent(player2);
    }
}

function textMessageEvent(player){
    if(player.character.stats.inventory.text.switch){
        player.character.stats.inventory.text.switch = false;
        setTimeout(function(){
            player.character.stats.inventory.text.removeText();
            player.character.stats.inventory.text.switch = true;
        }, 1000);
    }
}