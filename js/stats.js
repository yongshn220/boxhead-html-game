class Stats{
    name;
    type;
    state;
    fullHealthPoint;
    curHealthPoint;
    attackDamage;
    defense;
    experience;
    moveSpeed;
    inventory;
    life;

    constructor(name, type, life){
        this.name = name;
        this.type = type;
        this.state = true;
        this.fullHealthPoint = 1000;
        this.curHealthPoint = 1000;
        this.attackDamage = 0;
        this.defense = 0;
        this.experience = 0;
        this.moveSpeed = 0;
        this.life = life;
        this.inventory = new Inventory(name, type);
        this.updateInventory();
        this.showHealth();

    }
    gainHealthPoint(n){
        if(this.curHealthPoint + n >= this.fullHealthPoint){
            this.curHealthPoint = this.fullHealthPoint;
        }
        else{
            this.curHealthPoint += n;
        }
        this.showHealth();
    }

    
    damaged(attackDamage){
        var h = Math.floor(this.curHealthPoint - (attackDamage * ((100 - this.defense) / 100)));
        if(h <= 0){
            this.curHealthPoint = 0;
            this.state = false; //dead
        }
        else{
            this.curHealthPoint = h;
        }
        this.showHealth();
    }

    showHealth(){
        var curHealthBar = 800 * (this.curHealthPoint / this.fullHealthPoint);
        $("#health" + this.name).css("width", curHealthBar);
    }

    showInfo(){
        this.updateInventory();
        var level = document.getElementById("level" + this.name);
        var hp = document.getElementById("HP" + this.name);
        var attackDamage = document.getElementById("attackDamage" + this.name);
        var defense = document.getElementById("defense" + this.name);
        var life = document.getElementById("life" + this.name);

        level.innerHTML = "level: " + 0;
        hp.innerHTML = "HP: " + this.curHealthPoint;
        attackDamage.innerHTML = "AD: " + this.attackDamage;
        defense.innerHTML = "defense: " + this.defense + "%";
        life.innerHTML = "life: " + this.life;
    }

    updateInventory(){
        this.updateAttackDamage();
        this.updateDefense();
        this.updateMoveSpeed();
    }

    updateAttackDamage(){
        this.attackDamage = this.inventory.curWeapon.attackDamage;
    }
    updateDefense(){
        this.defense = this.inventory.curHelmet.defense + this.inventory.curArmor.defense + this.inventory.curShoes.defense;
    }
    updateMoveSpeed(){
        this.moveSpeed = this.inventory.curShoes.moveSpeed;
        if(this.inventory.curWeapon.name === "shield"){
            this.moveSpeed = this.moveSpeed / 2;
            this.defense = 98;
        }
    }
}