class Text{
    texts;
    switch;
    constructor(){
        this.texts = [];
        this.switch = true;
    }

    addText(name){
        this.texts.push(name);
        this.limitCheck();
    }

    removeText(){
        this.texts.splice(0, 1);
    }

    getText(){
        var curText = "++ ";
        for(var i = 0; i < this.texts.length; i++){
            curText += this.texts[i];
            if(i + 1 !== this.texts.length){
                curText += "  /  ";
            }
        }
        curText += " ++";
        return curText;
    }

    isEmpty(){
        if(this.texts.length <= 0){
            return true;
        }
        return false;
    }

    limitCheck(){
        while(this.texts.length >= 5){
            this.texts.splice(0, 1);
        }
    }
}