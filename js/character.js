class Character{
    name;
    type;
    sx;
    sy;
    sw;
    sh;
    dx;
    dy;
    dw;
    dh;
    gx;
    gy;
    image;
    view;
    isMoving;
    isAttacking;
    stats;
    background;

    constructor(src, name, type, background){
        this.name = name;
        this.type = type;
        this.isMoving = false;
        this.isAttacking = false;
        this.image = new Image();
        this.image.src = src;
        this.stats = new Stats(name, type, 200);
        this.background = background;
        this.setPosition();
    }

    getOppositeView(){
        if(this.view === "left"){
            return "right";
        }
        else if(this.view === "right"){
            return "left";
        }
        else if(this.view === "up"){
            return "down";
        }
        else if(this.view === "down"){
            return "up";
        }
        else{
            
        }
    }

    setPosition(){
        if(this.name === "A" && this.type === "player"){
            this.gx = 0;
            this.gy = 0;
            this.dx = 0;
            this.dy = 0;
            this.background.x = this.gx - this.dx;
            this.background.y = this.gy - this.dy;
        }
        else if(this.name === "B" && this.type === "player"){
            this.gx = 1700;
            this.gy = 1700;
            this.dx = 600;
            this.dy = 400;
            //this.gx = 1780;
            //this.gy = 1780;
            //this.dx = 600;
            //this.dy = 400;
            this.background.x = this.gx - this.dx;
            this.background.y = this.gy - this.dy;
        }
        this.sx = 0;
        this.sy = 0;
        this.sw = 80;
        this.sh = 80;
        this.dw = 80;
        this.dh = 80;
    }

    imageUpdate(){
        if(this.isMoving){
            if(this.sx === 0){
                this.sx = 80;
            }
            else{
                this.sx = 0;
            }
        }
    }

    viewUpdate(view){
        this.view = view;
        if(this.stats.state){
            if(view === "down"){
                this.sy = 0;
            }
            else if(view === "left"){
                this.sy = 80;
            }
            else if(view === "up"){
                this.sy = 160;
            }
            else if(view === "right"){
                this.sy = 240;
            }
        }
    }

    showNewWeapon(){
        var curWepaon = this.stats.inventory.curWeapon.name;
        this.image.src = 'css/images/characters/char' + this.name + '-' + curWepaon + '.png';
    }

    dead(){
        this.sx = this.sy;
        this.sy = 0;
        this.image.src = 'css/images/characters/char' + this.name + '-dead.png';
    }

    newLife(curLife){
        this.stats.state = true;
        if(this.type === "player"){
            this.newPosition();
        }
        this.stats = new Stats(this.name, this.type, curLife);
        var curWepaon = this.stats.inventory.curWeapon.name;
        this.image.src = 'css/images/characters/char' + this.name + '-' + curWepaon + '.png';
    }

    newPosition(){
        var x;
        var y;
        var notCollied = false;

        while(!notCollied){
            x = Math.floor(Math.random() * 1900);
            y = Math.floor(Math.random() * 1900);

            //x = 1690;
            //y = 1290;

            var tempXL = Math.floor(x / 40);
            var tempXR = Math.floor((x + this.dw) / 40);
            var tempYU = Math.floor(y / 40);
            var tempYD = Math.floor((y + this.dh) / 40);
            if(this.background.wallMap[tempYU][tempXL] ||
            this.background.wallMap[tempYU][tempXR] || 
            this.background.wallMap[tempYD][tempXL] || 
            this.background.wallMap[tempYD][tempXR]){
                notCollied = false;
            }
            else{
                notCollied = true;
                this.gx = x;
                this.gy = y;
                this.dPositionAdjust(this.gx, this.gy);
                this.background.x = this.gx - this.dx;
                this.background.y = this.gy - this.dy;
            }
        }
    }

    dPositionAdjust(gx, gy){
        var dx;
        var dy;

        if(gx < 400){
            dx = gx
        }
        else if(gx > 1600){
            dx = gx - 1199;
        }
        else{
            dx = 400;
        }
        if(gy < 400){
            dy = gy;
        }
        else if(gy > 1600){
            dy = gy - 1399;
        }
        else{
            dy = 400;
        }

        this.dx = dx;
        this.dy = dy;
    }
}